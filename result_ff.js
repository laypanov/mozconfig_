try{
lockPref('network.proxy.share_proxy_settings', true)
lockPref('network.proxy.http', '')
lockPref('network.proxy.http_port', 3128)
lockPref('network.proxy.https', '')
lockPref('network.proxy.https_port', 3128)
lockPref('network.proxy.ftp', '')
lockPref('network.proxy.ftp_port', 3128)
lockPref('network.proxy.socks', '')
lockPref('network.proxy.socks_port', 0)
lockPref('network.proxy.ssl', '')
lockPref('network.proxy.ssl_port', 3128)
lockPref('network.proxy.type', 1)
lockPref('network.proxy.no_proxies_on', 'localhost, 127.0.0.1,')
} catch(e) {
displayError('lockedPref', e);
}