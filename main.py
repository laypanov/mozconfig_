#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from ldap3 import Server, Connection, SUBTREE
from wsgiref.util import setup_testing_defaults
from wsgiref.simple_server import make_server


def simple_app(environ, start_response):
    setup_testing_defaults(environ)
    settings = ""
    status = '200 OK'
    headers = [('Content-type', 'text/plain; charset=utf-8')]

    start_response(status, headers)

    env = environ['PATH_INFO'][1:].split("/")
    if env[0] == "tb":
        username = env[1]
        compname = env[2]
        ad_data = search_ad(username)
        settings = settings_th(username, ad_data, compname)
    elif env[0] == "ff":
        settings = settings_ff()

    out = "try{\r%s\r} catch(e) {\rdisplayError('lockedPref', e);\r}" % settings
    return [out.encode('utf-8')]


def settings_ff():
    out_static = [
        "lockPref('network.proxy.share_proxy_settings', true)",
        "lockPref('network.proxy.http', '')",
        "lockPref('network.proxy.http_port', 3128)",
        "lockPref('network.proxy.https', '')",
        "lockPref('network.proxy.https_port', 3128)",
        "lockPref('network.proxy.ftp', '')",
        "lockPref('network.proxy.ftp_port', 3128)",
        "lockPref('network.proxy.socks', '')",
        "lockPref('network.proxy.socks_port', 0)",
        "lockPref('network.proxy.ssl', '')",
        "lockPref('network.proxy.ssl_port', 3128)",
        "lockPref('network.proxy.type', 1)",
        "lockPref('network.proxy.no_proxies_on', 'localhost, 127.0.0.1')",
    ]
    return '\n'.join(map(str, out_static))


def search_ad(username):
    ad_server = ''
    ad_user = ''
    ad_password = ''
    ad_search_tree = ''

    server = Server(ad_server)
    conn = Connection(server, user=ad_user, password=ad_password)
    conn.bind()

    conn.search(ad_search_tree, '(&(objectCategory=Person)(sAMAccountName=%s))' % username, SUBTREE,
                attributes=['givenName', 'sn', 'displayName', 'company', 'department', 'initials', 'ipPhone', 'mail',
                            'mobile', 'title']
                )
    user_info = [str(conn.entries[0]['givenName']), str(conn.entries[0]['sn']), str(conn.entries[0]['displayName']),
                 str(conn.entries[0]['company']), str(conn.entries[0]['department']), str(conn.entries[0]['initials']),
                 str(conn.entries[0]['ipPhone']), str(conn.entries[0]['mail']), str(conn.entries[0]['mobile']),
                 str(conn.entries[0]['title'])]
    return user_info


def settings_th(username, ad, cn):
    print(username, ad, cn)
    out_dynamic = []
    host_mail = ''
    domain_mail = ''
    i = 0

    # Добавление по ПК
    if cn == "TORGOVIZAL1":
        mail = [username, "post1", 'post2', 'post3', 'post4', 'post5']
    elif cn == "TZ2":
        mail = [username, "post1", "post2", 'post3', 'post4', 'post5']
    elif cn == "WS-09":
        mail = [username, "post1", "post2", 'post3', 'post4', 'post5']
    elif cn == "TZ4":
        mail = [username, "post1", "post2", 'post3', 'post4', 'post5']
    elif cn == "TZ3":
        mail = [username, "post5", "fax_rnd"]
    elif cn == "WS-21":
        mail = [username, "post1", "post2", 'post3', 'post4', 'post5']
    elif cn == "DESKTOP-EK4GDM5":
        mail = [username, "buh"]
    else:
        mail = [username]

    # Добавление по имени учетки
    if username == "irinam":
        mail = [username, "buh04"]


    un_ab = username

    out_static = ["lockPref('mail.shell.checkDefaultClient', true)",
                  "defaultPref('ldap_2.servers.domain.description', '%s.local')" % host_mail,
                  "defaultPref('ldap_2.servers.domain.uri', 'ldap:///ou=people,dc=don,dc=??sub?(objectclass=*)')",
                  "lockPref('network.proxy.type', 0)",
                  "defaultPref('ldap_2.autoComplete.directoryServer', 'ldap_2.servers.domain')",
                  "defaultPref('ldap_2.autoComplete.useDirectory', true)",
                  "lockPref('ldap_2.servers.domain.auth.saslmech', '')",
                  "lockPref('mail.spotlight.firstRunDone', true)",
                  "defaultPref('mail.spotlight.enable', false)",
                  "lockPref('ldap_2.servers.domain.maxHits', 300)",
                  "lockPref('ldap_2.servers.domain.filename', 'empl.mab')",
                  "defaultPref('ldap_2.servers.domain.auth.dn', 'uid=%s,ou=people,dc=don,dc=,dc=ru')" % un_ab,
                  "lockPref('extensions.update.enabled', true)",
                  "lockPref('mail.winsearch.firstRunDone', true)",
                  "lockPref('mail.startup.enabledMailChekOnce', true)",
                  "defaultPref('calendar.timezone.local', 'Europe/Moscow')",
                  "defaultPref('calendar.ui.version', 1)",
                  "defaultPref('calendar.view.dayendhour', 18)",
                  "defaultPref('calendar.view.daystarthour', 9)",
                  "defaultPref('calendar.week.start', 1)",
                  "lockPref('mail.server.default.autosync_max_age_days',15)",
                  "lockPref('app.update.enabled', false)",
                  "lockPref('browser.search.update', false)",
                  "lockPref('extensions.update.enabled', false)",
                  "defaultPref('mail.rights.version', 1)",
                  "defaultPref('mailnews.start_page.url', '')",

                  # сжатие папок
                  "defaultPref('mail.purge_threshhold_mb', 20)",
                  "defaultPref('prompt_purge_threshhold', false)",
                  "defaultPref('purge.ask', false)",
                  "defaultPref('mail.warn_on_collapsed_thread_operation', false)",
                  "defaultPref('mail.operate_on_msgs_in_collapsed_threads', true)",
                  ]
    # отключаем автопрочтение писем в ТЗ
    if cn == "TORGOVIZAL1" or cn == "TZ2" or cn == "TZ3" or cn == "TZ4" or cn == "WS-09":
        out_static.append("defaultPref('mailnews.mark_message_read.delay', false)")
        out_static.append("defaultPref('mailnews.mark_message_read.auto', false)")
        # out_static.append("defaultPref('ldap_2.autoComplete.useDirectory', true)")

    # подменяем если нужно логин пользователя
    for u in mail:
        i += 1
        if u == 'kofnovvv':
            un = 'fsc_don'
        else:
            un = u

        if u == 'post1' or u == 'post2' or u == 'post3' or u == 'post4' or u == 'post5':
            ad_un = 'Пост %s' % u[-1:]
        elif un == 'fax_rnd':
            ad_un = 'Факс РНД'

        else:
            ad_un = ad[2]

        out_dynamic_u = [

            "lockPref('mail.accountmanager.defaultaccount', '%s')" % u,
            "lockPref('mail.accountmanager.localfoldersserver', 'server%s')" % i,
            "lockPref('mail.account.%s.server', 'server%s')" % (u, i),
            "lockPref('mail.account.%s.identities', '%s')" % (u, u),

            "lockPref('mail.smtpserver.%s.try_ssl', 3)" % u,
            "lockPref('mail.smtpserver.%s.username', '%s')" % (u, u),
            "lockPref('mail.smtpserver.%s.port', 465)" % u,
            "lockPref('mail.smtpserver.%s.description', '%s')" % (u, u),
            "lockPref('mail.smtpserver.%s.auth_method', 1)" % u,
            "lockPref('mail.smtpserver.%s.hostname', '%s')" % (u, host_mail),

            "lockPref('mail.server.server%s.login_at_startup', true)" % i,
            "lockPref('mail.server.server%s.name', '%s@%s')" % (i, u, domain_mail),
            "lockPref('mail.server.server%s.type', 'imap')" % i,
            "lockPref('mail.server.server%s.realhostname', '%s')" % (i, host_mail),
            "lockPref('mail.server.server%s.userName', '%s')" % (i, u),
            "lockPref('mail.server.server%s.realuserName', '%s')" % (i, u),
            "lockPref('mail.server.server%s.hostname', '%s')" % (i, host_mail),
            "lockPref('mail.server.server%s.port', 993)" % i,
            "lockPref('mail.server.server%s.isSecure', true)" % i,
            "lockPref('mail.server.server%s.socketType', 3)" % i,
            "lockPref('mail.server.server%s.moveOnSpam', false)" % i,
            "lockPref('mail.server.server%s.moveTargetMode', 0)" % i,

            "lockPref('mail.identity.%s.fullName', '%s')" % (u, ad_un),
            "lockPref('mail.identity.%s.useremail', '%s@%s')" % (u, un, domain_mail),
            "lockPref('mail.identity.%s.smtpServer', '%s')" % (u, u),

            "lockPref('mail.identity.%s.reply_on_top', 1)" % u,
        ]
        out_dynamic = out_dynamic + out_dynamic_u
    mail = ','.join(map(str, mail))
    out_mult = ["lockPref('mail.accountmanager.accounts', '%s')" % mail,
                "lockPref('mail.smtpservers', '%s')" % mail,
                ]

    data = out_static + out_dynamic + out_mult
    return '\n'.join(map(str, data))


httpd = make_server('', 8085, simple_app)
httpd.serve_forever()
